#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define KERNEL_FILE "main_kernel.cl"	/* File where the GPU function resides */
#define KERNEL_FUNC "process_data"		/* Name of the GPU function */

#define GRID_RESOLUTION 10				/* The number of cells in each dimension of the grid */

/* Generic wrappers around the requested data types (e.g. crime & lights out) */
typedef struct
{
	double x;	/* Latitude of the data point */
	double y;	/* Longitude of the data point */
} DataType;

int main(int argc, char* argv[])
{
	/* ********************* */
	/* FUNCTION DECLARATIONS */
	/* ********************* */
	cl_device_id create_device();
	cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);
	long read_data(char* filename, DataType* array, double* min_x, double* max_x, double* min_y, double* max_y);
	void print_grid(int* grid);
	void print_array(DataType* array, long size);
	long get_file_line_count(char* filename);
	
	/* ********************************* */
	/* CPU PROCESSING & DATA PREPARATION */
	/* ********************************* */

	long file1length = get_file_line_count(argv[1]);
	long file2length = get_file_line_count(argv[2]);

	/* Declare dataset-related variables */
	DataType* type_a_array = (DataType*) malloc(sizeof(DataType) * file1length);
	DataType* type_b_array = (DataType*) malloc(sizeof(DataType) * file2length);

	/* Read data from files into arrays */
	double a_min_x, a_max_x, a_min_y, a_max_y;
	long a_size = read_data(argv[1], type_a_array, &a_min_x, &a_max_x, &a_min_y, &a_max_y);
	//print_array(type_a_array, a_size);

	double b_min_x, b_max_x, b_min_y, b_max_y;
	long b_size = read_data(argv[2], type_b_array, &b_min_x, &b_max_x, &b_min_y, &b_max_y);
	//print_array(type_b_array, b_size);
	
	/* Determine grid dimensions */
	double grid_min_x = a_min_x < b_min_x ? a_min_x : b_min_x;
	double grid_min_y = a_min_y < b_min_y ? a_min_y : b_min_y;
	double grid_max_x = a_max_x > b_max_x ? a_max_x : b_max_x;
	double grid_max_y = a_max_y > b_max_y ? a_max_y : b_max_y;

	/* Allocate memory for the GRID_RESOLUTION x GRID_RESOLUTION grid array */
	int grid_size = GRID_RESOLUTION * GRID_RESOLUTION;
	int grid[grid_size];
	int grid_final[grid_size];
	int i;
	for (i = 0; i < grid_size; i++)
		grid[i] = 0;

	/* ********************************** */
	/* GPU PROCESSING & DATA MANIPULATION */
	/* ********************************** */

	/* Declare OpenCL structures */
	cl_device_id device;
	cl_context context;
	cl_program program;
	cl_kernel kernel;
	cl_command_queue queue;
	cl_mem a_buffer, a_min_x_buffer, a_max_x_buffer, a_min_y_buffer, a_max_y_buffer, a_size_buffer;
	cl_mem b_buffer, b_min_x_buffer, b_max_x_buffer, b_min_y_buffer, b_max_y_buffer, b_size_buffer;
	cl_mem grid_buffer, grid_min_x_buffer, grid_max_x_buffer, grid_min_y_buffer, grid_max_y_buffer, grid_resolution_buffer;
	
	cl_int err;
	size_t local_size;		/* work-items (i.e. CUDA threads) per work-group */
	size_t global_size;		/* work-items (i.e. CUDA thread blocks) per kernel */
	cl_int num_groups;

	/* Create device and context */
	printf("Creating device & context...\n");
	device = create_device();
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	if (err != CL_SUCCESS)
	{
			perror("Couldn't create a context");
			exit(1); 
	}
	printf("\tSuccess!\n");

	/* Build program */
	printf("Building program...\n");
	program = build_program(context, device, KERNEL_FILE);
	printf("\tSuccess!\n");

	/* Create data buffer */
	global_size = 10000;	/* I have no idea what to set this to */
	local_size = 1;		/* I have no idea what to set this to */
	num_groups = global_size / local_size;
	
	/* Create a buffer on the device (the GPU) for the grid matrix */
	printf("Creating buffers...\n");
	/* Buffer for the DataType array */
	a_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, file1length * sizeof(DataType), type_a_array, &err);
	a_size_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(long), &a_size, &err);
	/* Buffer for the DataType array */
	b_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, file2length * sizeof(DataType), type_b_array, &err);
	b_size_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(long), &b_size, &err);

	/* Buffers for the grid array and its min/max dimensions */
	grid_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, (GRID_RESOLUTION * GRID_RESOLUTION) * sizeof(int), grid, &err);
	grid_min_x_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(double), &grid_min_x, &err);
	grid_max_x_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(double), &grid_max_x, &err);
	grid_min_y_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(double), &grid_min_y, &err);
	grid_max_y_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(double), &grid_max_y, &err);
	double grid_res = (double) GRID_RESOLUTION;
	grid_resolution_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(double), &grid_res, &err);
	
	if (err != CL_SUCCESS)
	{
		perror("Couldn't create a buffer");
		exit(1);
	}
	printf("\tSuccess!\n");

	/* Create a command queue */
	printf("Creating command queue...\n");
	queue = clCreateCommandQueue(context, device, 0, &err);
	if (err != CL_SUCCESS) 
	{
			perror("Couldn't create a command queue");
			exit(1);   
	}
	printf("\tSuccess!\n");

	/* Create a kernel */
	printf("Creating kernel...\n");
	kernel = clCreateKernel(program, KERNEL_FUNC, &err);
	if (err != CL_SUCCESS)
	{
			perror("Couldn't create a kernel");
			exit(1);
	}
	printf("\tSuccess!\n");

	/* Create kernel arguments */
	printf("Creating kernel arguments...\n");
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &a_buffer);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &a_size_buffer);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &b_buffer);
	err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &b_size_buffer);
	err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &grid_buffer);
	err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &grid_min_x_buffer);
	err |= clSetKernelArg(kernel, 6, sizeof(cl_mem), &grid_max_x_buffer);
	err |= clSetKernelArg(kernel, 7, sizeof(cl_mem), &grid_min_y_buffer);
	err |= clSetKernelArg(kernel, 8, sizeof(cl_mem), &grid_max_y_buffer);
	err |= clSetKernelArg(kernel, 9, sizeof(cl_mem), &grid_resolution_buffer);

	if(err != CL_SUCCESS)
	{
			perror("Couldn't create a kernel argument");
			exit(1);
	}
	printf("\tSuccess!\n");

	/* Enqueue kernel */
	printf("Enqueuing kernel...\n");
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL); 
	if(err != CL_SUCCESS)
	{
			perror("Couldn't enqueue the kernel");
			exit(1);
	}
	printf("\tSuccess!\n");

	/* Read the kernel's output */
	printf("Reading kernel output...\n");
	err = clEnqueueReadBuffer(queue, grid_buffer, CL_TRUE, 0, sizeof(int)*grid_size, grid_final, 0, NULL, NULL);
	if(err < 0)
	{
			perror("Couldn't read the buffer");
			exit(1);
	}
	printf("\tSuccess!\n");
	print_grid(grid_final);

	/* Deallocate resources */
	clReleaseKernel(kernel);
	clReleaseMemObject(a_buffer);
	clReleaseMemObject(a_size_buffer);
	clReleaseMemObject(b_buffer);
	clReleaseMemObject(b_size_buffer);
	clReleaseMemObject(grid_buffer);
	clReleaseMemObject(grid_min_x_buffer);
	clReleaseMemObject(grid_max_x_buffer);
	clReleaseMemObject(grid_min_y_buffer);
	clReleaseMemObject(grid_max_y_buffer);
	clReleaseMemObject(grid_resolution_buffer);
	clReleaseCommandQueue(queue);
	clReleaseProgram(program);
	clReleaseContext(context);
	return 0;
}

long get_file_line_count(char* filename)
{
	FILE* myfile = fopen(filename, "r");
	int ch;
	long number_of_lines = 0;

	do 
	{
		ch = fgetc(myfile);
		if(ch == '\n')
			number_of_lines++;
	} while (ch != EOF);

	// last line doesn't end with a new line!
	// but there has to be a line at least before the last line
	if(ch != '\n' && number_of_lines != 0) 
		number_of_lines++;

	fclose(myfile);

	return number_of_lines;
}

/*
 * Read the dataset file with the DataType data and populate type_a_array
 * Return the number of items read
 * Should be modified to do dynamic allocation
 */
long read_data(char* filename, DataType* array, double* min_x, double* max_x, double* min_y, double* max_y)
{
	/* Dummy declarations */
	long idx = 0;
	/*
	 * Open file with data for a type
	 * Assume the following format (for now?)
	 * 		x1,y1
	 * 		x2,y2
	 * 		...
	 * Increment idx first thing in every loop iteration, so idx = count always
	 * Maintain up-to-date values of min_x, max_x, min_y, max_y
	 * Add each new struct to array (which is really type_a_array)
	*/

	FILE *stream = fopen(filename, "r");
	char line[1024] = {0x0};
	while(fgets(line, 1024, stream) != 0)
	{
		DataType entry;
		// parse each line for the three inputs
		char *tmp = strdup(line);
		char *token;

		token = strtok(tmp, ",");
		entry.x = atof(token);
		if (idx == 0) 
			*min_x = entry.x;
		if (entry.x < *min_x)
			*min_x = entry.x;
		if (idx == 0)
			*max_x = entry.x;
		if (entry.x > *max_x)
			*max_x = entry.x;

		token = strtok(NULL, ",");
		entry.y = atof(token);
		if (idx == 0) 
			*min_y = entry.y;
		if (entry.y < *min_y)
			*min_y = entry.y;
		if (idx == 0)
			*max_y = entry.y;
		if (entry.y > *max_y)
			*max_y = entry.y;

		array[idx] = entry;	
		idx++;
	}
	return idx;
}


/* **************** */
/* HELPER FUNCTIONS */
/* **************** */

/* Find a GPU or CPU associated with the first available platform */
cl_device_id create_device()
{
		cl_platform_id platform;
		cl_device_id dev;
		int err;

		/* Identify a platform (e.g. NVIDIA system) */
		err = clGetPlatformIDs(1, &platform, NULL);
		if(err < 0)
		{
				perror("Couldn't identify a platform");
				exit(1);
		} 

		/* Access a device (i.e. the first GPU on the platform) */
		err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
		if(err == CL_DEVICE_NOT_FOUND)
		{
				err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
		}
		if(err < 0)
		{
				perror("Couldn't access any devices");
				exit(1);   
		}
		return dev;
}

/* Create program from a file and compile it */
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename)
{
		cl_program program;
		FILE *program_handle;
		char *program_buffer, *program_log;
		size_t program_size, log_size;
		int err;

		/* Read program file and place content into buffer */
		program_handle = fopen(filename, "r");
		if(program_handle == NULL) {
				perror("Couldn't find the program file");
				exit(1);
		}
		fseek(program_handle, 0, SEEK_END);
		program_size = ftell(program_handle);
		rewind(program_handle);
		program_buffer = (char*)malloc(program_size + 1);
		program_buffer[program_size] = '\0';
		fread(program_buffer, sizeof(char), program_size, program_handle);
		fclose(program_handle);

		/* Create program from file */
		program = clCreateProgramWithSource(ctx, 1, 
						(const char**)&program_buffer, &program_size, &err);
		if(err < 0) {
				perror("Couldn't create the program");
				exit(1);
		}
		free(program_buffer);

		/* Build program */
		err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
		if(err < 0) {

				/* Find size of log and print to std output */
				clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
								0, NULL, &log_size);
				program_log = (char*) malloc(log_size + 1);
				program_log[log_size] = '\0';
				clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
								log_size + 1, program_log, NULL);
				printf("%s\n", program_log);
				free(program_log);
				exit(1);
		}
		return program;
}

/* Print an array of structs of DataType or DataType */
void print_array(DataType* array, long size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		printf("[x = %f] [y = %f]\n", array[i].x, array[i].y);
		if (i == size - 1)
			printf("*** *** ***\n");
	}
}

void print_grid(int* grid)
{
	int index;
	printf("*** *** ***\n");
	for (index = 0; index < GRID_RESOLUTION * GRID_RESOLUTION; index++)
		index % GRID_RESOLUTION == 0 ? printf("\n%d", grid[index]) : printf("\t%d", grid[index]);
	printf("\n");
}
