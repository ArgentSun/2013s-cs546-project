
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
	long ID;	/* Unique identifier of the data point */
	double x;	/* Latitude of the data point */
	double y;	/* Longitude of the data point */
} DataType;

long read_data(char* filename, DataType* array, double* min_x, double* max_x, double* min_y, double* max_y);

int main(int argc, char* argv[])
{
	DataType* entries = (DataType*) malloc(sizeof(DataType) * 10);
	double min_x, min_y, max_x, max_y;

	printf("Getting data from file...");
	long count = read_data(argv[1], entries, &min_x, &max_x, &min_y, &max_y);
	printf("DONE!\n");

	printf("Looping through %ld ENTRIES...\n", count);
	int i;
	for(i=0; i<10;i++)
		printf("ID: %ld\tX:%f\tY:%f\n", entries[i].ID, entries[i].x, entries[i].y);
}


long read_data(char* filename, DataType* array, double* min_x, double* max_x, double* min_y, double* max_y)
{
	/* Dummy declarations */
	*min_x = 0;
	*max_x = 0;
	*min_y = 0;
	*max_y = 0;
	int idx = 0;
	/*
	 * Open file with data for a type
	 * Assume the following format (for now?)
	 * 		x1,y1
	 * 		x2,y2
	 * 		...
	 * Increment idx first thing in every loop iteration, so idx = count always
	 * Maintain up-to-date values of min_x, max_x, min_y, max_y
	 * Add each new struct to array (which is really type_a_array)
	*/

	FILE *stream = fopen(filename, "r");
	char line[1024] = {0x0};
	while(fgets(line, 1024, stream) != 0)
	{
		DataType entry;
		entry.ID = idx;
		// parse each line for the three inputs
		char *tmp = strdup(line);
		int i = 0;
		char *token;

		token = strtok(tmp, ",");
		entry.x = atof(token);
		if (entry.x < *min_x)
			*min_x = entry.x;
		if (entry.x > *max_x)
			*max_x = entry.x;

		token = strtok(NULL, ",");
		entry.y = atof(token);
		if (entry.y < *min_y)
			*min_y = entry.y;
		if (entry.y > *max_y)
			*max_y = entry.y;

		array[idx] = entry;	
		idx++;
	}

	return idx;
}
