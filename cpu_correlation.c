#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define GRID_RESOLUTION 10				/* The number of cells in each dimension of the grid */

typedef struct
{
	//long ID;	/* Unique identifier of the data point */
	double x;	/* Latitude of the data point */
	double y;	/* Longitude of the data point */
} DataType;

long read_data(char* filename,DataType* array, double* min_x, double* max_x, double* min_y, double* max_y);
void print_grid(int* grid);
void print_array(DataType* array, long size);
long get_file_line_count(char* filename);

int main(int argc, char** argv) {

	long file1length = get_file_line_count(argv[1]);
	long file2length = get_file_line_count(argv[2]);
	long max_file_length = file1length < file2length ? file2length : file1length;

	DataType* type_a_array = (DataType*) malloc(sizeof(DataType) * file1length);
	DataType* type_b_array = (DataType*) malloc(sizeof(DataType) * file2length);

	double a_min_x, a_max_x, a_min_y, a_max_y;
	long a_size = read_data(argv[1], type_a_array, &a_min_x, &a_max_x, &a_min_y, &a_max_y);
	//print_array(type_a_array, a_size);

	double b_min_x, b_max_x, b_min_y, b_max_y;
	long b_size = read_data(argv[2], type_b_array, &b_min_x, &b_max_x, &b_min_y, &b_max_y);
	//print_array(type_b_array, b_size);

	double grid_min_x = a_min_x < b_min_x ? a_min_x : b_min_x;
	double grid_min_y = a_min_y < b_min_y ? a_min_y : b_min_y;
	double grid_max_x = a_max_x > b_max_x ? a_max_x : b_max_x;
	double grid_max_y = a_max_y > b_max_y ? a_max_y : b_max_y;

	long grid_size = GRID_RESOLUTION * GRID_RESOLUTION; 
	int grid[grid_size];
	long i;
	for (i = 0; i < grid_size; i++)
		grid[i] = 0;

	double grid_res = (double) GRID_RESOLUTION;
	double grid_cell_width = (grid_max_x - grid_min_x) / grid_res;
	double grid_cell_height = (grid_max_y - grid_min_y) / grid_res;
	long x_idx, y_idx;

	for(i = 0; i < a_size; i++)
	{
		x_idx = (long) ((type_a_array[i].x - grid_min_x) / grid_cell_width);
		y_idx = (long) ((type_a_array[i].y - grid_min_y) / grid_cell_height);
		grid[(x_idx * GRID_RESOLUTION) + y_idx]++;
	}


	for(i = 0; i < b_size; i++)
	{
		x_idx = (long) ((type_b_array[i].x - grid_min_x) / grid_cell_width);
		y_idx = (long) ((type_b_array[i].y - grid_min_y) / grid_cell_height);
		grid[(x_idx * GRID_RESOLUTION) + y_idx]--;
	}
	print_grid(grid);
}

long get_file_line_count(char* filename)
{
	FILE* myfile = fopen(filename, "r");
	int ch, number_of_lines = 0;

	do 
	{
		ch = fgetc(myfile);
		if(ch == '\n')
			number_of_lines++;
	} while (ch != EOF);

	// last line doesn't end with a new line!
	// but there has to be a line at least before the last line
	if(ch != '\n' && number_of_lines != 0) 
		number_of_lines++;

	fclose(myfile);

	return number_of_lines;
}

long read_data(char* filename, DataType* array, double* min_x, double* max_x, double* min_y, double* max_y)
{
	/* Dummy declarations */
	long idx = 0;
	/*
	 * Open file with data for a type
	 * Assume the following format (for now?)
	 * 		x1,y1
	 * 		x2,y2
	 * 		...
	 * Increment idx first thing in every loop iteration, so idx = count always
	 * Maintain up-to-date values of min_x, max_x, min_y, max_y
	 * Add each new struct to array (which is really type_a_array)
	 */

	FILE *stream = fopen(filename, "r");
	char line[1024] = {0x0};
	while(fgets(line, 1024, stream) != 0)
	{
		DataType entry;
		//entry.ID = idx;
		// parse each line for the three inputs
		char *tmp = strdup(line);
		char *token;

		token = strtok(tmp, ",");
		entry.x = atof(token);
		if (idx == 0) 
			*min_x = entry.x;
		if (entry.x < *min_x)
			*min_x = entry.x;
		if (idx == 0)
			*max_x = entry.x;
		if (entry.x > *max_x)
			*max_x = entry.x;

		token = strtok(NULL, ",");
		entry.y = atof(token);
		if (idx == 0) 
			*min_y = entry.y;
		if (entry.y < *min_y)
			*min_y = entry.y;
		if (idx == 0)
			*max_y = entry.y;
		if (entry.y > *max_y)
			*max_y = entry.y;

		array[idx] = entry;	
		idx++;
	}

	return idx;
}

void print_array(DataType* array, long size)
{
	long i;
	for (i = 0; i < size; i++)
	{
		printf("[x = %f] [y = %f]\n", array[i].x, array[i].y);
		if (i == size - 1)
			printf("*** *** ***\n");
	}
}

void print_grid(int* grid)
{
	int index;
	printf("*** *** ***\n");
	for (index = 0; index < GRID_RESOLUTION * GRID_RESOLUTION; index++)
		index % GRID_RESOLUTION == 0 ? printf("\n%d", grid[index]) : printf("\t%d", grid[index]);
	printf("\n");
}
