/* The kernel function */

#pragma OPENCL EXTENSION cl_khr_fp64 : enable		/* Needed for OpenCL compiler to recognize type double */
typedef struct
{
    double x;   /* Latitude of the data point */
    double y;   /* Longitude of the data point */
} DataType;

__kernel void process_data( 
							  __global DataType* a
							, __global long* a_size
							, __global DataType* b
							, __global long* b_size
							, __global int* grid
							, __global double* grid_min_x
							, __global double* grid_max_x
							, __global double* grid_min_y
							, __global double* grid_max_y
							, __global double* grid_resolution
						  )
{
	double grid_cell_step = (*grid_max_x - *grid_min_x) / *grid_resolution;	/* step amount for each "row" in flattened array */
	double grid_cell_offset = (*grid_max_y - *grid_min_y) / *grid_resolution; /* offset from first index of "row" */

	int gid;
	gid = (int) get_global_id(0);

	/* gid ranges between 0 and (global_size - 1). This segment establishes strict bounds for each kernel execution */
	int max_gid_a = (*a_size / 10);
	int lower_bound_a = gid * 10;
	int upper_bound_a = (gid + 1) * 10;

	int max_gid_b = (*b_size / 10);
	int lower_bound_b = gid * 10;
	int upper_bound_b = (gid + 1) * 10;

	int i;
	/* This is where the grid gets updated */
/*
	if (gid < *a_size)
	{
		long x_idx, y_idx;
		x_idx = (long) ((a[i].x - *grid_min_x) / grid_cell_step);
		y_idx = (long) ((a[i].y - *grid_min_y) / grid_cell_offset);
		grid[(x_idx * (int)*grid_resolution) + y_idx]++;
	}
	
	if (gid < *b_size)
	{
		long x_idx, y_idx;
		x_idx = (long) ((b[i].x - *grid_min_x) / grid_cell_step);
		y_idx = (long) ((b[i].y - *grid_min_y) / grid_cell_offset);
		grid[(x_idx * (int)*grid_resolution) + y_idx]--;
	}
*/

	if (gid < max_gid_a)
	{
		for (i = lower_bound_a; i < upper_bound_a; i++)
		{
			long x_idx, y_idx;
			x_idx = (long) ((a[i].x - *grid_min_x) / grid_cell_step);
			y_idx = (long) ((a[i].y - *grid_min_y) / grid_cell_offset);
			grid[(x_idx * (int)*grid_resolution) + y_idx]++;
		}
	}

	int j;
	if (gid < max_gid_b)
	{
		for (j = lower_bound_b; j < upper_bound_b; j++)
		{
			long x_idx, y_idx;
			x_idx = (long) ((b[j].x - *grid_min_x) / grid_cell_step);
			y_idx = (long) ((b[j].y - *grid_min_y) / grid_cell_offset);
			grid[(x_idx * (int)*grid_resolution) + y_idx]--;
		}
	}
}
